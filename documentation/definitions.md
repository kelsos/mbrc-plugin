## Definitions
### TrackInfo
|Name|Description|Required|Schema|Default|
|----|----|----|----|----|
|code|A response code from the plugin API, it should be 200 for success|false|integer||
|album|The album of the playing track|false|string||
|artist|The artist of the playing track|false|string||
|title|The title of the playing track|false|string||
|year|The year in which the playing track was released|false|string||
|path|The path of the file in the filesystem|false|string||



﻿namespace MusicBeeRemoteCore.Rest.ServiceInterface
{
    public class ApiCodes
    {
        public static int Failure = 500;

        public static int Success = 200;
    }
}